# Ansible Playbook: ARM Devops

Installs bitbucket / jenkins / xl-dynamic / xl-release

## Requirements

Ansible 2.5

## Dependencies

cd roles
ansible-galaxy install -r requirements.yml -p .

## Running playbooks
ansible-playbook -i inventories/hklife/uat bitbucket.yml
ansible-playbook -i inventories/hklife/uat jenkins.yml
ansible-playbook -i inventories/hklife/uat xl-dynamic.yml
ansible-playbook -i inventories/hklife/uat xl-release.yml

Note: Do update inventory and group_vars before running the playbook
      bitbucket.yml file has been updated with task for bitbucket version update

